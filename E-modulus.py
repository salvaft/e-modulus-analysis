import sys
import argparse
import numpy as np
import xlrd
from scipy import stats
import matplotlib.backends.backend_qt5agg
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
from matplotlib.ticker import FormatStrFormatter
from PyQt5.QtWidgets import *
from PyQt5 import QtGui, QtCore

def get_slope(path):
    area = 294.6

    LVDTpos = [5, 11, 17, 23]
    barpos = [0, 4, 6, 10, 12, 16, 18, 22]
    sides = [[], []]
    LVDT = []
    stiffness = [[[0], [0]], [[0], [0]]]
    sidesavg = [0]
    stiffnessavg = [[0], [0]]
    book = xlrd.open_workbook(path)
    # print number of sheets print(book.nsheets)
    # print sheet names print(book.sheet_names())
    # get the first worksheet
    first_sheet = book.sheet_by_index(0)
    # read a row// print(first_sheet.row_values(0))
    # read a cell// cell = first_sheet.cell(0, 0)
    #print(cell.value)
    # read a row slice
    #print(first_sheet.ncols)
    """"
    DATA={}
    for i in range(first_sheet.ncols):
        COL = first_sheet.col_slice(colx=i, start_rowx=49, end_rowx=first_sheet.nrows)
        j=0
        values=[]
        for cell in COL:
            values.append(cell.value*1000/area)
            j=j+1
        DATA[first_sheet.cell(1,i).value] = values
    """
    for side in sides:
        if sides.index(side) == 0:
            s0 = barpos[0]
            s1 = barpos[1]
            s2 = barpos[2]
            s3 = barpos[3]
        elif sides.index(side) == 1:
            s0 = barpos[4]
            s1 = barpos[5]
            s2 = barpos[6]
            s3 = barpos[7]

        for i in range(first_sheet.nrows-49):
            suma = 0
            for value in first_sheet.row_values(rowx=i+49, start_colx=s0, end_colx=s1):
                suma = suma+value
            for value in first_sheet.row_values(rowx=i+49, start_colx=s2, end_colx=s3):
                suma = suma+value
            side.append(suma/10)

    for row in range(first_sheet.nrows - 49):
        suma = 0
        for pos in LVDTpos:
            suma = suma + first_sheet.cell(row+49, pos).value
        LVDT.append(suma/4)

    for j, side in enumerate(sides):
        for m, stress in enumerate(side):
            sidesavg.append((sides[0][m]+sides[1][m])/2)
            if stress >= args.interval[0] and stress < args.interval[1] and stress > stiffness[j][1][-1]:
                stiffness[j][0].append(LVDT[sides[j].index(stress)])
                stiffness[j][1].append(stress)
            if sidesavg[m] >= args.interval[0] and sidesavg[m] < args.interval[1] and sidesavg[m] > stiffnessavg[1][-1]:
                stiffnessavg[1].append(sidesavg[m])
                stiffnessavg[0].append(LVDT[sides[j].index(stress)])

    del side
    del sides
    del sidesavg
    del stiffnessavg[0][0]
    del stiffnessavg[1][0]
    for j, side in enumerate(stiffness):
        del side[0][0]
        del side[1][0]

    try:
        slopeSteel, _, _, _, _ = stats.linregress(np.asarray(side))
    except:
        print("There is no data for that interval")
        sys.exit()

    return slopeSteel, stiffness, stiffnessavg

def plot_curves(stiffness, stiffnessavg):
    plt.switch_backend('qt5agg')
    fig = plt.figure()
    ax = fig.add_subplot(111, label="1")
    ax.set_xlabel('mm')
    ax.set_ylabel('MPa')
    # ax2 = fig.add_subplot(111, label="2", frame_on=False)  this is done to use several axis in the same plot
    # plt.set_yticks(np.arange(interval[0], interval[1], freq))
    # ax2.set_yticks(np.arange(interval[0], interval[1], freq))
    # ax2.set_xticks([])
    # ax2.set_yticks([])
    ax.plot(stiffness[1][0], stiffness[1][1], 'o', color="C0", label="Right")
    ax.plot(stiffness[0][0], stiffness[0][1], 'o', color="C1", label="Left")
    ax.plot(stiffnessavg[0], stiffnessavg[1], 'o', color="C2", label="Avg")

    loc = plticker.MultipleLocator(
    base = (args.interval[1] - args.interval[0]) / 10)  # this locator puts ticks at regular intervals
    ax.yaxis.set_major_locator(loc)
    loc2 = plticker.MultipleLocator(
    base = (max(max(stiffness[0][0]), max(stiffness[1][0])) - min(min(stiffness[0][0]), min(stiffness[1][0]))) / 10)
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.xaxis.set_major_locator(loc2)

    plt.legend(shadow=True, fancybox=True)
    plt.show()

def get_emod(slopeSteel, slopeCoil):
    mc = 6225.82312
    b = 564.38133
    slopeSteelAnsys = 1176426.18649
    emod = ((((1/slopeCoil) - (1/slopeSteel) + (1/slopeSteelAnsys))**(-1)) - b)*(1/mc)
    print(emod)

class App(QWidget):
    def __init__(self):
        super(App, self).__init__()

        class LinesFrame(QFrame):
            def __init__(self, parentWidget):
                super(LinesFrame, self).__init__(parentWidget)
                lower_b = QLineEdit()
                upper_b = QLineEdit()
                grid = QGridLayout()
                l1 = QLabel("Lower bound", self)
                l2 = QLabel("Upper bound", self)
                grid.addWidget(l1, 0, 0, QtCore.Qt.AlignCenter)
                grid.addWidget(l2, 1, 0, QtCore.Qt.AlignCenter)
                grid.addWidget(lower_b, 0, 1, QtCore.Qt.AlignCenter)
                grid.addWidget(upper_b, 1, 1, QtCore.Qt.AlignCenter)
                self.setLayout(grid)
        def initUI(self):
            self.setWindowTitle("E-mod for 11T")
            self.openFileNameDialog()
            self.frame = LinesFrame(self)
            self.plot_get = QPushButton("Run")
            self.plot_get.setFixedSize(150, 50)
            self.grid = QGridLayout()
            self.grid.addWidget(self.frame, 0, 0)
            self.grid.addWidget(self.plot_get, 1, 0, QtCore.Qt.AlignCenter)
            self.setLayout(self.grid)

        initUI(self)
        self.show()
        slopeCoil, stiffness_coil, stiffnessavg_coil = get_slope(args.coil)
        slopeSteel, _, _ = get_slope(args.steel)
        get_emod(slopeSteel, slopeCoil)

        self.plot_get.clicked.connect(plot_curves(stiffness_coil, stiffnessavg_coil))


    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.steel, _ = QFileDialog.getOpenFileName(self, "Steel .xls", "",
                                                 "All Files (*);;Python Files (*.py)", options=options)
        self.coil, _ = QFileDialog.getOpenFileName(self, "Coil .xls", "",
                                                 "All Files (*);;Python Files (*.py)", options=options)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--steel', type=str, default='TEKSCAN.xls',
                        help='Path to the .xls file of the calibration')
    parser.add_argument('--coil', type=str, default='TEKSCAN.xls',
                        help='Path to the .xls file of the measurement of the coil')
    parser.add_argument('--interval', type=list, default=[0, 1],
                        help='A list of two values with the lower bound and upper bound, in this order')

    global args
    args = parser.parse_args()

    #app = QApplication(sys.argv)
    #ex = App()
    try:
        if ex.steel and ex.coil:
            args.steel = ex.steel
            args.coil = ex.coil
            sys.exit(app.exec_())
        else:
            slopeCoil, stiffness_coil, stiffnessavg_coil = get_slope(args.coil)
            slopeSteel, _, _ = get_slope(args.steel)
            get_emod(slopeSteel, slopeCoil)
            plot_curves(stiffness_coil, stiffnessavg_coil)
    except:
        slopeCoil, stiffness_coil, stiffnessavg_coil = get_slope(args.coil)
        slopeSteel, _, _ = get_slope(args.steel)
        get_emod(slopeSteel, slopeCoil)
        plot_curves(stiffness_coil, stiffnessavg_coil)

